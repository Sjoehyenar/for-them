﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

	public class PlayerController : MonoBehaviour {
		
	// Left & Right movement initialization 
		private Rigidbody2D myrigidbody;
		public float speed;
		public float jumpForce;
		private float moveInput;
		
	// Jump initialization
		private bool isGrounded;
		public Transform feetPos;
		public float checkRadius;
		public LayerMask whatIsGround;
		
	// Jump limitation initialization 
		private float jumpTimeCounter;
		public float jumpTime;
		private bool isJumping;

		void Start (){
			myrigidbody = GetComponent<Rigidbody2D>();
		}
		
		void FixedUpdate(){
			moveInput = Input.GetAxisRaw("Horizontal");
			myrigidbody.velocity = new Vector2(moveInput * speed, myrigidbody.velocity.y);
		}
		
		void Update (){
			isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, whatIsGround);
			
	// Player orientation point (Euler Angles) during jump-state 
			if(moveInput > 0){
				transform.eulerAngles = new Vector3(0, 0, 0);
			} else if(moveInput < 0){
				transform.eulerAngles = new Vector3(0, 180, 0);
			}
			
	// If you're on the ground, then you can jump
			if(isGrounded == true && Input.GetKeyDown(KeyCode.Space)){
	
	// You remain in the air-jump state during a definite jumpTime.
				isJumping = true; 
				jumpTimeCounter = jumpTime;
				myrigidbody.velocity = Vector2.up * jumpForce;
			}
	
			if(Input.GetKey(KeyCode.Space) && isJumping == true){
				
	// You can jump higher in the air, but not a double jump
				if(jumpTimeCounter > 0){
				myrigidbody.velocity = Vector2.up * jumpForce;
				jumpTimeCounter -= Time.deltaTime;
			} else {
				isJumping = false;
			}
		}
	
	// You are not in the air-jump state 
		if(Input.GetKeyUp(KeyCode.Space)){
			isJumping = false; 
		}
	}
}